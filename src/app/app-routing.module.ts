import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent }   from './components/dashboard/dashboard.component';
import { VilleComponent }   from './components/ville/ville.component';
import { ParkingComponent }   from './components/parking/parking.component';
import { ParkingListComponent }   from './components/parking-list/parking-list.component';

const routes: Routes = [
  {
    path: '', 
    component: DashboardComponent
  },
  {
    path: 'dashboard', 
    component: DashboardComponent
  },
  {
    path: ':ville', 
    component: ParkingListComponent
  },
  {
    path: ':ville/:parking', 
    component: ParkingComponent
  },
  {
    path: '**', 
    component: DashboardComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
