import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chart-available-places',
  templateUrl: './chart-available-places.component.html',
  styleUrls: ['./chart-available-places.component.scss']
})
export class ChartAvailablePlacesComponent implements OnInit {

  @Input()
  totalPlaces:number;

  @Input()
  availablePlaces:number;

  barChartOptions = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
  };

  public barChartLabels = ["Occupée", "Libres"]; 
  public barChartType = 'pie';
  public barChartLegend = true;

  public barChartData = [
    {data: [], label: '', borderColor: '#42a7ff', backgroundColor: 'rgba(0, 0, 0, 0.1)', hoverBackgroundColor: []},
  ];

  constructor() { }

  ngOnInit() {
    this.barChartData[0].data.push(this.totalPlaces-this.availablePlaces);
    this.barChartData[0].data.push(this.availablePlaces);
  }

}
