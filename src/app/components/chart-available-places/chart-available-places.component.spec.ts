import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartAvailablePlacesComponent } from './chart-available-places.component';

describe('ChartAvailablePlacesComponent', () => {
  let component: ChartAvailablePlacesComponent;
  let fixture: ComponentFixture<ChartAvailablePlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartAvailablePlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartAvailablePlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
