import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityVignetComponent } from './city-vignet.component';

describe('CityVignetComponent', () => {
  let component: CityVignetComponent;
  let fixture: ComponentFixture<CityVignetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityVignetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityVignetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
