import { Component, OnInit, Input  } from '@angular/core';

/**
 * Use exemple :
 * <app-city-vignet cityName="Nantes" nbParking="132" class="city-vignet-yellow"></app-city-vignet>
 * <app-city-vignet cityName="Rennes" nbParking="456" class="city-vignet-red"></app-city-vignet>
 * <app-city-vignet cityName="Angers" nbParking="789" class="city-vignet-blue"></app-city-vignet>
 */
@Component({
  selector: 'app-city-vignet',
  templateUrl: './city-vignet.component.html',
  styleUrls: ['./city-vignet.component.scss']
})
export class CityVignetComponent implements OnInit {

  @Input() cityName:string;
  @Input() nbParking:number;
  @Input() class:string;
  constructor() { }

  ngOnInit() {
  }

}
