import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphDashboardPlacesComponent } from './graph-dashboard-places.component';

describe('GraphDashboardPlacesComponent', () => {
  let component: GraphDashboardPlacesComponent;
  let fixture: ComponentFixture<GraphDashboardPlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphDashboardPlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphDashboardPlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
