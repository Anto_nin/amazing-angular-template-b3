import { Component, OnInit } from '@angular/core';
import { NantesService } from '../../services/nantes-service/nantes.service';
import { RennesService } from '../../services/rennes-service/rennes.service';
import { AngersService } from '../../services/angers-service/angers.service';

import {DashboardParking} from '../../models/dashborad-parking/dashboard-parking.model';
import * as SCSS_VARS from '../../scss-variables.generated.json';

@Component({
  selector: 'app-graph-dashboard-places',
  templateUrl: './graph-dashboard-places.component.html',
  styleUrls: ['./graph-dashboard-places.component.scss']
})
export class GraphDashboardPlacesComponent implements OnInit {

  constructor(
    private nantesService:NantesService,
    private rennesService:RennesService,
    private angersService:AngersService
    ) { }
  datas: Array<DashboardParking>;

  barChartOptions = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    scales: {
      xAxes: [{
        gridLines: {
          display:false
        }
      }],
      yAxes: [{
        gridLines: {
            color: "$l-text",
            maxLines: 5
        }   
      }]
    }
  };

  public barChartLabels = []; // nantes, rennes, angers
  public barChartType = 'bar';
  public barChartLegend = false;

  public barChartData = [
    {data: [], label: 'Total places', backgroundColor: [], hoverBackgroundColor: []},
    {data: [], label: 'Available places', backgroundColor: [], hoverBackgroundColor: []}
  ];

  ngOnInit() {
    this.datas = [];
    this.loadDatas().then((datas)=> {
      this.datas = datas;
      this.bindDatas();
    });
    this.datas.push();
   

    
  }

  bindDatas() {
    this.datas.forEach((parking) => {
      this.barChartLabels.push(parking.name);
      this.barChartData[0].data.push(parking.totalPlaces);
      this.barChartData[0].backgroundColor.push(SCSS_VARS["default"][parking.colorLight])
      this.barChartData[0].hoverBackgroundColor.push(SCSS_VARS["default"][parking.colorLight])
      this.barChartData[1].data.push(parking.actualPlaces);
      this.barChartData[1].hoverBackgroundColor.push(SCSS_VARS["default"][parking.colorClass])
      this.barChartData[1].backgroundColor.push(SCSS_VARS["default"][parking.colorClass])
      
    })
  }

  async loadDatas():Promise<any>{
    let datas = [];
    let nbPlacesNantes = await this.nantesService.getNbPlacesTotal();
    let avPlaceNantes = await this.nantesService.getNbPlacesAvailable();
    datas.push(new DashboardParking('Nantes', nbPlacesNantes, avPlaceNantes, "$yello", "$lyello"))
    let nbPlacesRennes = await this.rennesService.getNbPlacesTotal();
    let avPlaceRennes = await this.rennesService.getNbPlacesAvailable();
    datas.push(new DashboardParking('Rennes', nbPlacesRennes, avPlaceRennes, "$red", "$lred"));
    
    let nbPlacesAngers = await this.angersService.getNbPlacesTotal();
    let avPlaceAngers = await this.angersService.getNbPlacesAvailable();
    datas.push(new DashboardParking('Angers', nbPlacesAngers, avPlaceAngers, "$blue", "$lblue"));
    
    return datas;
  }

}
