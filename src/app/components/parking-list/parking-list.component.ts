import { Component, OnInit, ChangeDetectorRef  } from '@angular/core';
import { Parking } from '../../models/parking/parking';
import { NantesService } from '../../services/nantes-service/nantes.service';
import { RennesService } from '../../services/rennes-service/rennes.service';
import { AngersService } from '../../services/angers-service/angers.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-parking-list',
  templateUrl: './parking-list.component.html',
  styleUrls: ['./parking-list.component.scss']
})
export class ParkingListComponent implements OnInit {

  constructor(
    private nantesService:NantesService,
    private rennesService:RennesService,
    private angersService:AngersService,
    private route: ActivatedRoute,
    private ref: ChangeDetectorRef
  ) { }

  parkings:Parking[];
  free:number = 0;
  paying:number = 0;
  city:string;

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.city = this.route.snapshot.paramMap.get("ville").toUpperCase();
      this.loadDatas();
    })
  }

  async loadDatas() {
    let nbParking = 0;
    switch(this.city) {
      case 'NANTES': 
        nbParking = await this.nantesService.getNbParking();
        this.free = await this.nantesService.getFreeParking();
        this.paying = nbParking - this.free;
        this.parkings = await this.nantesService.getParkingCityDatas();
      break;
      case 'RENNES': 
        nbParking = await this.rennesService.getNbParking();
        this.free = await this.rennesService.getFreeParking();
        this.paying = nbParking - this.free;
        this.parkings = await this.rennesService.getParkingCityDatas();
      break;
      case 'ANGERS': 
        nbParking = await this.angersService.getNbParking();
        this.free = await this.angersService.getFreeParking();
        this.paying = nbParking - this.free;
        this.parkings = await this.angersService.getParkingCityDatas();
      break;
    }
    this.ref.markForCheck();
  }

}
