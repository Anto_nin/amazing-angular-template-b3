import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }       from '@angular/router';
import { NantesService } from '../../services/nantes-service/nantes.service';
import { RennesService } from '../../services/rennes-service/rennes.service';
import { AngersService } from '../../services/angers-service/angers.service';
import { Identifiers } from '@angular/compiler';

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.scss']
})
export class ParkingComponent implements OnInit {

  parkingName: string;
  ville: string;
  parkingId: string;
  horaires:any;


  constructor(
    private route: ActivatedRoute,
    private nantesService:NantesService,
    private rennesService:RennesService,
    private angersService:AngersService) { }

  ngOnInit( ) {
    this.horaires = [];
    this.horaires.push({day: 'lundi', start: "", end: ""}); 
    this.horaires.push({day: 'mardi', start: "", end: ""});
    this.horaires.push({day: 'mercredi', start: "", end: ""});
    this.horaires.push({day: 'jeudi', start: "", end: ""});
    this.horaires.push({day: 'vendredi', start: "", end: ""});
    this.horaires.push({day: 'samedi', start: "", end: ""});
    this.horaires.push({day: 'dimanche', start: "", end: ""});
    this.ville = this.route.snapshot.paramMap.get('ville');
    this.parkingId = this.route.snapshot.paramMap.get('parking'); 

    this.getInfosFromVille()
  }

  getInfosFromVille() {
    switch (this.ville) {
      case 'NANTES':
        this.nantesService.getHoraires(this.parkingId)
        .then(resp=> {
          this.parkingName = resp.fields.nom;
          this.horaires.find(horaire => horaire.day == 'lundi').start = resp.fields.heure_debut;
          this.horaires.find(horaire => horaire.day == 'lundi').end = resp.fields.heure_fin;
          this.horaires.find(horaire => horaire.day == 'mardi').start = resp.fields.heure_debut;
          this.horaires.find(horaire => horaire.day == 'mardi').end = resp.fields.heure_fin;
          this.horaires.find(horaire => horaire.day == 'mercredi').start = resp.fields.heure_debut;
          this.horaires.find(horaire => horaire.day == 'mercredi').end = resp.fields.heure_fin;
          this.horaires.find(horaire => horaire.day == 'jeudi').start = resp.fields.heure_debut;
          this.horaires.find(horaire => horaire.day == 'jeudi').end = resp.fields.heure_fin;
          this.horaires.find(horaire => horaire.day == 'vendredi').start = resp.fields.heure_debut;
          this.horaires.find(horaire => horaire.day == 'vendredi').end = resp.fields.heure_fin;
          this.horaires.find(horaire => horaire.day == 'samedi').start = resp.fields.heure_debut;
          this.horaires.find(horaire => horaire.day == 'samedi').end = resp.fields.heure_fin;
          this.horaires.find(horaire => horaire.day == 'dimanche').start = resp.fields.heure_debut;
          this.horaires.find(horaire => horaire.day == 'dimanche').end = resp.fields.heure_fin;
        })
        .catch(error=>console.log(error));
        break;
      case 'RENNES':
        this.rennesService.getHoraires(this.parkingId).then((res)=> {
          this.horaires.find(horaire => horaire.day == 'lundi').start = res.fields.orgahoraires;
          this.horaires.find(horaire => horaire.day == 'lundi').end = "";
          this.horaires.find(horaire => horaire.day == 'mardi').start = res.fields.orgahoraires;
          this.horaires.find(horaire => horaire.day == 'mardi').end = "";
          this.horaires.find(horaire => horaire.day == 'mercredi').start = res.fields.orgahoraires;
          this.horaires.find(horaire => horaire.day == 'mercredi').end = "";
          this.horaires.find(horaire => horaire.day == 'jeudi').start = res.fields.orgahoraires;
          this.horaires.find(horaire => horaire.day == 'jeudi').end = "";
          this.horaires.find(horaire => horaire.day == 'vendredi').start = res.fields.orgahoraires;
          this.horaires.find(horaire => horaire.day == 'vendredi').end = "";
          this.horaires.find(horaire => horaire.day == 'samedi').start = res.fields.orgahoraires;
          this.horaires.find(horaire => horaire.day == 'samedi').end = "";
          this.horaires.find(horaire => horaire.day == 'dimanche').start = res.fields.orgahoraires;
          this.horaires.find(horaire => horaire.day == 'dimanche').end = "";
        })
      break;
      case 'ANGERS':
        this.angersService.getHoraires(this.parkingId).then((res)=> {
          this.horaires.find(horaire => horaire.day == 'lundi').start = res.fields.horaires_ouverture;
          this.horaires.find(horaire => horaire.day == 'lundi').end = res.fields.horaires_fermeture;
          this.horaires.find(horaire => horaire.day == 'mardi').start = res.fields.horaires_ouverture;
          this.horaires.find(horaire => horaire.day == 'mardi').end = res.fields.horaires_fermeture;
          this.horaires.find(horaire => horaire.day == 'mercredi').start = res.fields.horaires_ouverture;
          this.horaires.find(horaire => horaire.day == 'mercredi').end = res.fields.horaires_fermeture;
          this.horaires.find(horaire => horaire.day == 'jeudi').start = res.fields.horaires_ouverture;
          this.horaires.find(horaire => horaire.day == 'jeudi').end = res.fields.horaires_fermeture;
          this.horaires.find(horaire => horaire.day == 'vendredi').start = res.fields.horaires_ouverture;
          this.horaires.find(horaire => horaire.day == 'vendredi').end = res.fields.horaires_fermeture;
          this.horaires.find(horaire => horaire.day == 'samedi').start = res.fields.horaires_ouverture;
          this.horaires.find(horaire => horaire.day == 'samedi').end = res.fields.horaires_fermeture;
          this.horaires.find(horaire => horaire.day == 'dimanche').start = res.fields.horaires_ouverture;
          this.horaires.find(horaire => horaire.day == 'dimanche').end = res.fields.horaires_fermeture;
          this.horaires.find(horaire => horaire.day == 'mardi').start = res.fields.orgahoraires;
        })
      break;
      default:
        this.parkingName = 'Pas de nom';
        this.horaires.find(horaire => horaire.day == 'lundi').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'lundi').end = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'mardi').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'mardi').end = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'mercredi').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'mercredi').end = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'jeudi').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'jeudi').end = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'vendredi').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'vendredi').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'vendredi').end = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'samedi').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'samedi').end = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'dimanche').start = "Pas d'heure";
        this.horaires.find(horaire => horaire.day == 'dimanche').end = "Pas d'heure";
        break;
    }
  }


}
