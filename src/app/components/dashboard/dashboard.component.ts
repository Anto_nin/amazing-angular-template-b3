import { Component, OnInit } from '@angular/core';
import { NantesService } from '../../services/nantes-service/nantes.service';
import { RennesService } from '../../services/rennes-service/rennes.service';
import { AngersService } from '../../services/angers-service/angers.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private nantesService:NantesService,
    private rennesService:RennesService,
    private angersService:AngersService
    ) { }

  nantesDatas = { cityName: 'Nantes', nbParking: 0 };
  rennesDatas = { cityName: 'Rennes', nbParking: 0 };
  angersDatas = { cityName: 'Angers', nbParking: 0 };

  ngOnInit() {
    this.nantesService.getNbParking().then((v) => {
      this.nantesDatas.nbParking = v;
    })

    this.rennesService.getNbParking().then((v) => {
      this.rennesDatas.nbParking = v
    })

    this.angersService.getNbParking().then((v) => {
      this.angersDatas.nbParking = v
    })
    
  }

}
