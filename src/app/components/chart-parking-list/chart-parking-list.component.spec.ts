import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartParkingListComponent } from './chart-parking-list.component';

describe('ChartParkingListComponent', () => {
  let component: ChartParkingListComponent;
  let fixture: ComponentFixture<ChartParkingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartParkingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartParkingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
