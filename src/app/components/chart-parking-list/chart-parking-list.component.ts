import { Component, OnInit, Input } from '@angular/core';
import * as SCSS_VARS from '../../scss-variables.generated.json';

/**
 * <app-chart-parking-list paying="50" free="25"></app-chart-parking-list>
 */

@Component({
  selector: 'app-chart-parking-list',
  templateUrl: './chart-parking-list.component.html',
  styleUrls: ['./chart-parking-list.component.scss']
})
export class ChartParkingListComponent implements OnInit {

  constructor() { }

  @Input()
  paying:number;
  @Input()
  free:number;

  barChartOptions = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: true,
    responsive: true,
  };

  public barChartLabels = ["Gratuits", "Payants"]; 
  public barChartType = 'pie';
  public barChartLegend = true;

  public barChartData = [
    {data: [], label: ['Gratuits', 'Payants'], backgroundColor: [], hoverBackgroundColor: []},
  ];

  ngOnInit() {
    this.loadData();
  }

  ngOnChanges() {
    this.loadData();
  }

  loadData() {
    this.barChartData[0].data = [];
    this.barChartData[0].data.push(this.free);
    this.barChartData[0].backgroundColor.push(SCSS_VARS["default"]['$lblue']);
    this.barChartData[0].data.push(this.paying);
    this.barChartData[0].backgroundColor.push(SCSS_VARS["default"]['$lred']);
  }

}
