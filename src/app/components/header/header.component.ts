import { Component, OnInit } from '@angular/core';
import { $ } from 'protractor';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  activeNav: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  navActive(){
    this.activeNav = !this.activeNav;
  }

}
