import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/services/generic-service/generic.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chart-availability-in-period',
  templateUrl: './chart-availability-in-period.component.html',
  styleUrls: ['./chart-availability-in-period.component.scss']
})
export class ChartAvailabilityInPeriodComponent implements OnInit {

  city: string;
  parkingId: string;
  startDate: Date = new Date(2019, 5, 1, 0, 0, 0, 0);
  endDate: Date = new Date(2019, 9, 1, 0, 0, 0, 0);

  // Chart Properties
  public chartOptions = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    backgroundColor: '#EEE'
  };

  public chartLabels = [];
  public chartType = 'line';
  public chartLegend = false;

  public chartData = [
    {data: [], label: ''},
  ];

  constructor(
    private genericService: GenericService,
    private route: ActivatedRoute
    ) {}

  ngOnInit() {
    this.setPropertiesWithRouteParams();
    this.getAvailabilityInPeriod();
  }

  // Start date handler
  startDateChange(event) {
    console.log('[ChartAvailabilityInPeriodComponent] - start date change', event.value);
    this.startDate = new Date(event.value);
    this.getAvailabilityInPeriod();
  }

  // End date handler
  endDateChange(event) {
    console.log('[ChartAvailabilityInPeriodComponent] - end date change', event.value);
    this.endDate = new Date(event.value);
    this.getAvailabilityInPeriod();
  }

  // get url params to set some component properties
  private setPropertiesWithRouteParams() {
    this.city = this.route.snapshot.paramMap.get('ville');
    this.parkingId = this.route.snapshot.paramMap.get('parking');
  }

  // get chart data
  private getAvailabilityInPeriod() {
    this.genericService.getAvailabilityInPeriod(this.city, this.parkingId, this.startDate, this.endDate)
    .subscribe( (data: any) => {
      this.chartData[0].data = data.data;
      this.chartLabels = data.labels;
    })
  }

}
