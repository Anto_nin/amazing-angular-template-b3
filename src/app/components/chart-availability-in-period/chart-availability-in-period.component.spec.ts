import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartAvailabilityInPeriodComponent } from './chart-availability-in-period.component';

describe('ChartAvailabilityInPeriodComponent', () => {
  let component: ChartAvailabilityInPeriodComponent;
  let fixture: ComponentFixture<ChartAvailabilityInPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartAvailabilityInPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartAvailabilityInPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
