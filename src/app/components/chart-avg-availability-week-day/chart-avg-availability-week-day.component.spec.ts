import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartAvgAvailabilityWeekDayComponent } from './chart-avg-availability-week-day.component';

describe('ChartAvgAvailabilityWeekDayComponent', () => {
  let component: ChartAvgAvailabilityWeekDayComponent;
  let fixture: ComponentFixture<ChartAvgAvailabilityWeekDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartAvgAvailabilityWeekDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartAvgAvailabilityWeekDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
