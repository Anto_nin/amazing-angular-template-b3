import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/services/generic-service/generic.service';
import { City } from 'src/app/enums/city.enum';
import { ActivatedRoute } from '@angular/router';
import { ThemeService } from 'ng2-charts';

@Component({
  selector: 'app-chart-avg-availability-week-day',
  templateUrl: './chart-avg-availability-week-day.component.html',
  styleUrls: ['./chart-avg-availability-week-day.component.scss']
})
export class ChartAvgAvailabilityWeekDayComponent implements OnInit {

  city: string;
  parkingId: string;
  selectedWeekNum = 1;

  public chartOptions = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    backgroundColor: '#EEE'
  };

  public chartLabels = [];
  public chartType = 'line';
  public chartLegend = false;

  public chartData = [
    {data: [], label: ''},
  ];

  public days = [
    {value: '1', viewValue: 'Lundi'},
    {value: '2', viewValue: 'Mardi'},
    {value: '3', viewValue: 'Mercredi'},
    {value: '4', viewValue: 'Jeudi'},
    {value: '5', viewValue: 'Vendredi'},
    {value: '6', viewValue: 'Samedi'},
    {value: '0', viewValue: 'Dimanche'},
  ];

  constructor(
    private genericService: GenericService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.generateChartLabels();
    this.setPropertiesWithRouteParams();
    this.getAverageAvailabilityForWeekNum();
  }

  public onSelectChange(value) {
    this.selectedWeekNum = value;
    this.getAverageAvailabilityForWeekNum();
  }

  private setPropertiesWithRouteParams() {
    this.city = this.route.snapshot.paramMap.get('ville');
    this.parkingId = this.route.snapshot.paramMap.get('parking');
  }

  private getAverageAvailabilityForWeekNum() {
    this.genericService.getAverageAvailabilityForWeekNum(this.city, this.parkingId, this.selectedWeekNum).subscribe( (data:any) => {
      this.chartData[0].data = data.averageAvailabilities;
    })
  }

  private generateChartLabels() {
    for (let i = 0; i < 24; i++) {
      this.chartLabels.push(i);
    }
  }

}
