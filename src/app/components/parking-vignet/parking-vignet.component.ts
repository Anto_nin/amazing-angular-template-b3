import { Component, OnInit, Input } from '@angular/core';

/**
 * <app-parking-vignet name="alpha" availiblePlaces="2" totalPlaces="5" ></app-parking-vignet>
 * <app-parking-vignet name="beta" availiblePlaces="2" totalPlaces="10" ></app-parking-vignet>
 * <app-parking-vignet name="gamma" availiblePlaces="2" totalPlaces="100" ></app-parking-vignet>
 */

@Component({
  selector: 'app-parking-vignet',
  templateUrl: './parking-vignet.component.html',
  styleUrls: ['./parking-vignet.component.scss']
})
export class ParkingVignetComponent implements OnInit {

  constructor() { }

  @Input()
  name:string;
  @Input()
  ville:string;
  @Input()
  parkingId:string;
  @Input()
  availiblePlaces:number;
  @Input()
  totalPlaces: number;

  color:string;

  ngOnInit() {
    if(this.availiblePlaces/this.totalPlaces > 0.3){
      this.color = "green";
    } else if (this.availiblePlaces/this.totalPlaces > 0.1) {
      this.color = "yello";
    } else {
      this.color = "red";
    }
  }

}
