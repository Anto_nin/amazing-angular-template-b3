import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkingVignetComponent } from './parking-vignet.component';

describe('ParkingVignetComponent', () => {
  let component: ParkingVignetComponent;
  let fixture: ComponentFixture<ParkingVignetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkingVignetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingVignetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
