import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { MatIconModule } from "@angular/material/icon";
import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { VilleComponent } from './components/ville/ville.component';
import { ParkingComponent } from './components/parking/parking.component';
import { CityVignetComponent } from './components/city-vignet/city-vignet.component';
import { GraphDashboardPlacesComponent } from './components/graph-dashboard-places/graph-dashboard-places.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ParkingVignetComponent } from './components/parking-vignet/parking-vignet.component';
import { ChartParkingListComponent } from './components/chart-parking-list/chart-parking-list.component';
import { ParkingListComponent } from './components/parking-list/parking-list.component';
import { ChartAvailablePlacesComponent } from './components/chart-available-places/chart-available-places.component';
import { ChartAvgAvailabilityWeekDayComponent } from './components/chart-avg-availability-week-day/chart-avg-availability-week-day.component';
import { ChartAvailabilityInPeriodComponent } from './components/chart-availability-in-period/chart-availability-in-period.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    VilleComponent,
    ParkingComponent,
    DashboardComponent,
    CityVignetComponent,
    GraphDashboardPlacesComponent,
    DashboardComponent,
    ParkingVignetComponent,
    ChartParkingListComponent,
    ParkingListComponent,
    ChartAvailablePlacesComponent,
    ChartAvgAvailabilityWeekDayComponent,
    ChartAvailabilityInPeriodComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    MatIconModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }