export class Parking {
    parkingId: String;
    name: String;
    availablePlaces: number;
    totalPlaces: number;
}
