export class DashboardParking {
    name:string;
    totalPlaces:number;
    actualPlaces:number;
    colorClass:string;
    colorLight:string;
    constructor(name, tPlaces, aPlaces, color, colorLight) {
        this.name = name;
        this.totalPlaces = tPlaces;
        this.actualPlaces = aPlaces;
        this.colorClass = color;
        this.colorLight = colorLight;
    }
}
