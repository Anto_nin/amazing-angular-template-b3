import { TestBed } from '@angular/core/testing';

import { AngersService } from './angers.service';

describe('AngersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AngersService = TestBed.get(AngersService);
    expect(service).toBeTruthy();
  });
});
