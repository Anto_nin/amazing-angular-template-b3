import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Parking } from '../../models/parking/parking';
@Injectable({
  providedIn: 'root'
})
export class AngersService {
  url:string = "https://data.angers.fr/api/records/1.0/search/?dataset=parking-angers&rows=20" 
  urlPlaces:string = "https://data.angers.fr/api/records/1.0/search/?dataset=pv_equip_parking&rows=20"
  urlParkingInfo:string = "https://data.angers.fr/api/records/1.0/search/?dataset=pv_equip_parking&rows=20";
  
  constructor(private http: HttpClient) { }

  async getNbParking():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url)
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.length;
        resolve(firstValue);
      });
    });
  }


  async getNbPlacesTotal():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.urlPlaces)
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => {return (record.fields.nb_places) ? record.fields.nb_places : 0; })
        .reduce( (accu, value) =>{
          return accu+value;
        });
        resolve(firstValue);
      });
    });
  }

  async getNbPlacesAvailable():Promise<Object> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url)
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => record.fields.disponible).reduce( (accu, value) =>{
          return accu+value;
        });
        resolve(firstValue);
      });
    });
  }

  async getFreeParking():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.urlParkingInfo+"&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => {return record.fields.exploitant ? 0 : 1 }).reduce( (accu, value) =>{
          return accu+value;
        });
        subscription = this.http.get(this.urlParkingInfo+"&start=20")
        .subscribe((data:any) => {
          let array = data.records.map(record => {return record.fields.exploitant ? 0 : 1 });
          let secd = array.length ? array.reduce( (accu, value) =>{
            return accu+value;
          }) : 1;
          let result:number = secd+firstValue;
          subscription.unsubscribe();
          resolve(result);
        });
      });
    })
  }

  async getParkingCityDatas() {
    let parking:Parking[] = [];
    let j = 0;
    let a;
    let records = [];

    while(a = await this.callUrl(this.urlPlaces, j)) {
      if(!a || j>50) {
        break;
      }
      records = records.concat(a.records);  // fields.id_parking
      j+=20
    }

    j = 0;
    while(a = await this.callUrl(this.url, j)) {
        if(!a || j>50) {
          break;
        }
        a.records.forEach(record => {
          let p:Parking = new Parking();
          p.name = record.fields.nom;
          p.totalPlaces = records.find((record) => {return record.fields.id_parking == p.name}).fields.nb_places;
          p.availablePlaces = record.fields.disponible
          p.parkingId = record.fields.nom
          parking.push(p);
        });
        j+=20
    }
    return parking;
  }

  async getHoraires(id: string):Promise<any> {
    let a;
    let j = 0;
    let datas = [];
    while(a = await this.callUrl(this.urlPlaces, j)) {
        if(!a || j>50) {
          break;
        }
        datas = datas.concat(a.records);
        j+=20;
    }
    return datas.find(record => record.fields.id_parking == id);
  }

  async callUrl(url, start) {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(url+"&start="+start)
      .subscribe((data:any) => {
        subscription.unsubscribe();
        resolve(data);
      });
    })
  }

}
