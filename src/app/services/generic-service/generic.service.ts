import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { City } from 'src/app/enums/city.enum';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  API_URL = 'https://parking.dany-corbineau.fr/api';

  constructor(private http: HttpClient) { }
  
  // Get average availability for a week num
  public getAverageAvailabilityForWeekNum(city: string, parkingId: string, weekNum: number) {
    return this.http.get(`${this.API_URL}/availability/${city}/${parkingId}/${weekNum}`);
  }

  // Get availability in period
  public getAvailabilityInPeriod(city: string, parkingId: string, startDate: Date, endDate: Date) {

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.API_URL}/availability/${city}/evolution-in-period/${parkingId}`, {startDate, endDate}, { headers });
  }
}
