import { TestBed } from '@angular/core/testing';

import { RennesService } from './rennes.service';

describe('RennesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RennesService = TestBed.get(RennesService);
    expect(service).toBeTruthy();
  });
});
