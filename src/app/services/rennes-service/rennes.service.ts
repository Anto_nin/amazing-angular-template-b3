import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Parking } from '../../models/parking/parking';

@Injectable({
  providedIn: 'root'
})
export class RennesService {
  url:string = "https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=export-api-parking-citedia" 
  urlParkingInfo:string = "https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=parkings&rows=20"
  constructor(private http: HttpClient) { }

  async getNbParking():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url+"&rows=20&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.length;
        subscription = this.http.get(this.url+"&rows=20&start=20")
        .subscribe((data:any) => {
          let result:number = data.records.length+firstValue;
          resolve(result);
        });
      });
    });
  }

  async getNbPlacesTotal():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url+"&rows=20&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => record.fields.max).reduce( (accu, value) =>{
          return accu+value;
        });
        resolve(firstValue);
      });
    });
  }

  async getNbPlacesAvailable():Promise<Object> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url+"&rows=20&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => record.fields.free).reduce( (accu, value) =>{
          return accu+value;
        });
        resolve(firstValue);
      });
    });
  }

  async getFreeParking():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.urlParkingInfo+"&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => {return record.fields.payant!=='Non' ? 0 : 1 }).reduce( (accu, value) =>{
          return accu+value;
        });
        subscription = this.http.get(this.urlParkingInfo+"&start=20")
        .subscribe((data:any) => {
          let array = data.records.map(record => {return record.fields.payant!=='Non' ? 0 : 1 });
          let secd = array.length ? array.reduce( (accu, value) =>{
            return accu+value;
          }) : 1;
          let result:number = secd+firstValue;
          subscription.unsubscribe();
          resolve(result);
        });
      });
    })
  }

  async getParkingCityDatas() {
    let parking:Parking[] = [];
    let j = 0;
    let a;
    while(a = await this.callUrl(this.url, j)) {
        if(!a || j>50) {
          break;
        }
        a.records.forEach(record => {
          let p:Parking = new Parking();
          p.name = record.fields.key;
          p.availablePlaces = record.fields.free
          p.totalPlaces = record.fields.max
          p.parkingId = record.fields.id;
          parking.push(p);
        });
        j+=20
    }
    return parking;
  }

  async getHoraires(id: string):Promise<any> {
    let a;
    let j = 0;
    let datas = [];
    while(a = await this.callUrl(this.url, j)) {
        if(!a || j>50) {
          break;
        }
        datas = datas.concat(a.records);
        j+=20;
    }
    return datas.find(record => record.fields.id == id);
  }

  async callUrl(url, start) {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(url+"&start="+start)
      .subscribe((data:any) => {
        subscription.unsubscribe();
        resolve(data);
      });
    })
  }

}
