import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Parking } from '../../models/parking/parking';

@Injectable({
  providedIn: 'root'
})
export class NantesService {

  url:string = "https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_parkings-publics-nantes-disponibilites" 
  urlParkingInfo:string = "https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_parkings-publics-nantes&rows=20"
  urlHoraires: string = "https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_parkings-publics-nantes-horaires&facet=nom_periode&facet=jour&facet=type_horaire";
  constructor(private http: HttpClient) { }

  async getNbParking():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url+"&rows=20&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.length;
        subscription = this.http.get(this.url+"&rows=20&start=20")
        .subscribe((data:any) => {
          let result:number = data.records.length+firstValue;
          resolve(result);
        });
      });
    });
  }

  async getNbPlacesTotal():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url+"&rows=20&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => record.fields.grp_exploitation).reduce( (accu, value) =>{
          return accu+value;
        });
        subscription = this.http.get(this.url+"&rows=20&start=20")
        .subscribe((data:any) => {
          let secd = data.records.map(record => record.fields.grp_exploitation).reduce( (accu, value) =>{
            return accu+value;
          });
          let result:number = secd+firstValue;
          subscription.unsubscribe();
          resolve(result);
        });
      });
    });
  }

  async getNbPlacesAvailable():Promise<Object> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.url+"&rows=20&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => record.fields.grp_disponible).reduce( (accu, value) =>{
          return accu+value;
        });
        subscription = this.http.get(this.url+"&rows=20&start=20")
        .subscribe((data:any) => {
          let secd = data.records.map(record => record.fields.grp_disponible).reduce( (accu, value) =>{
            return accu+value;
          });
          let result:number = secd+firstValue;
          subscription.unsubscribe();
          resolve(result);
        });
      });
    });
  }
  async getHoraires(id: string):Promise<any> {
      let a;
      let j = 0;
      let datas = [];
      while(a = await this.callUrl(this.urlHoraires, j)) {
          if(!a || j>50) {
            break;
          }
          datas = datas.concat(a.records);
          j+=20;
      }
      return datas.find(record => record.fields.idobj == id);
  }

  async getFreeParking():Promise<any> {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(this.urlParkingInfo+"&start=0")
      .subscribe((data:any) => {
        subscription.unsubscribe();
        let firstValue = data.records.map(record => {return record.fields.moyen_paiement ? 0 : 1 }).reduce( (accu, value) =>{
          return accu+value;
        });
        subscription = this.http.get(this.url+"&start=20")
        .subscribe((data:any) => {
          let secd = data.records.map(record => {return record.fields.moyen_paiement ? 0 : 1 }).reduce( (accu, value) =>{
            return accu+value;
          });
          let result:number = secd+firstValue;
          subscription.unsubscribe();
          resolve(result);
        });
      });
    })
  }

  async getParkingCityDatas() {
    let parking:Parking[] = [];
    let j = 0;
    let a;
    while(a = await this.callUrl(this.url, j)) {
        if(!a || j>50) {
          break;
        }
        a.records.forEach(record => {
          let p:Parking = new Parking();
          p.name = record.fields.grp_nom;
          p.availablePlaces = record.fields.grp_disponible
          p.totalPlaces = record.fields.grp_exploitation
          p.parkingId = record.fields.idobj
          parking.push(p);
        });
        j+=20
    }
    return parking;
  }

  async callUrl(url, start) {
    return new Promise((resolve, reject) => {
      let subscription = this.http.get(url+"&start="+start)
      .subscribe((data:any) => {
        subscription.unsubscribe();
        resolve(data);
      });
    })
  }

}
